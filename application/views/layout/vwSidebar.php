<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<?php 
				$icon ="fontello-users";
				$level = $this->session->userdata('level');
				foreach($this->m_menu->getAllMenu($level)->result() as $row){
				?>
				<li class="treeview">
					<a href="<?php echo site_url($row->URL_MENU) ?>"><i class="fa fa-files-o"></i><span><?php echo $row->NM_MENU?></span>
					<?php $count = $this->m_menu->getCountSubMenu($row->ID_MENU,$level)->result();
						foreach($count as $r){
							$con = $r->jumlah;
						}
						if($con > 0){ ?>
						<i class="fa fa-angle-left pull-right"></i>
						<?php }else{} ?>
						</a>
					<?php $count = $this->m_menu->getCountSubMenu($row->ID_MENU,$level)->result();
						foreach($count as $r){
							$con = $r->jumlah;
						}
						if($con > 0){ ?>
						
					<ul class="treeview-menu" >
						<?php
							foreach($this->m_menu->getAllSubMenu($row->ID_MENU,$level)->result() as $rows){
								?>
								<li>
									<a href="<?php echo site_url($rows->URL)?>"><i class="fa fa-circle-o"></i> <?php echo $rows->NM_SUBMENU?> </a>
								</li>
						<?php } ?>
					</ul>
					<?php }else{
					} ?>
				</li>
			<?php } ?>
		</ul>
	</section>
<!-- /.sidebar -->
</aside>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Create the tabs -->
	<ul class="nav nav-tabs nav-justified control-sidebar-tabs"></ul>
	<!-- Tab panes -->
	<div class="tab-content">
		<!-- Home tab content -->
		<div class="tab-pane" id="control-sidebar-home-tab">
		</div><!-- /.tab-pane -->
	</div>
</aside><!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
	immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>