<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Pengaturan
		<small>Level dan Hak Akses</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('welcome/login')?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active"><?php echo $dash; ?></li>
	</ol>
</section>

<!-- Main content -->

<section class="content">
	<!-- Info boxes -->
	<br/>
	<div class="row">
		<div class="col-md-4 col-sm-12">
			<div class="box box-primary ">
				<div class="box-header with-border">
					<h3 class="box-title">Hak Akses Admin</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
					</div>
				</div><!-- /.box-header -->
				<form action="<?php echo site_url('welcome/ubahHakAdmin') ?>" method="post">
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<ul class="" style='list-style:none'>
								<?php 
									$icon ="fontello-users";
									$level = 1;
									foreach($this->m_menu->getAllMenuAcl($level)->result() as $row){
									// Define a default Page
									  
									?>
									<li class="">
										<li>
											<div class="checkbox">
												<label>
												  <input type="checkbox" name='boxMenuAdmin[]' value="<?php echo $row->ID;?>" <?php echo $row->STTS =='1' ? 'checked readonly' : '' ?> onclick="saveDataAclUser(this.value)" />
												  <?php echo $row->NM_MENU?>
												</label>
											</div>
											
										</li>
										<?php $count = $this->m_menu->getCountSubMenu($row->ID_MENU,$level)->result();
											foreach($count as $r){
												$con = $r->jumlah;
											}
											if($con > 0){ ?>
											
										<ul class="" style='list-style:none'>
											<?php
												foreach($this->m_menu->getAllSubMenuAcl($row->ID_MENU,$level)->result() as $rows){
													?>
													<li>
														<div class="checkbox">
															<label>
															  <input type="checkbox" name='boxMenuAdmin[]' value="<?php echo $rows->ID;?>" <?php echo  $rows->STTS =='1' ? 'checked' : '' ?> onclick="saveDataAclUser(this.value)" /> 
															  <?php echo $rows->NM_SUBMENU?> 
															</label>
														</div>
													</li>
											<?php } ?>
										</ul>
										<?php }else{
										} ?>
									</li>
								<?php } ?>
								</ul>
							</div>
						</div><!-- /.row -->
					</div><!-- /.box-body -->
				</form>
			</div><!-- /.box -->
		</div>
		
		<div class="col-md-4 col-sm-12">
			<div class="box box-success ">
				<div class="box-header with-border">
					<h3 class="box-title">Hak Akses User</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
					</div>
				</div><!-- /.box-header -->
				<form action="#" method="post" id="formUser">
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<ul style='list-style:none' id='loadAclUser'>
								<?php 
									$icon ="fontello-users";
									$level = 2;
									foreach($this->m_menu->getAllMenuAcl($level)->result() as $row){
									?>
									<li>
										<li>
											<div class="checkbox">
												<label>
												  <input type="checkbox" name='boxMenuUser[]' onclick="saveDataAclUser(this.value)" value="<?php echo $row->ID;?>" <?php echo  $row->STTS =='1' ? 'checked' : '' ?>>
												  <?php echo $row->NM_MENU?> 
												</label>
											</div>
										</li>
										<?php $count = $this->m_menu->getCountSubMenu($row->ID_MENU,$level)->result();
											foreach($count as $r){
												$con = $r->jumlah;
											}
											if($con > 0){ ?>
											
										<ul class="" style='list-style:none'>
											<?php
												foreach($this->m_menu->getAllSubMenuAcl($row->ID_MENU,$level)->result() as $rows){
													?>
													<li>
														<div class="checkbox">
															<label>
															  <input type="checkbox" name='boxMenuUser[]' class='boxMenuUser[]' onclick="saveDataAclUser(this.value)" value="<?php echo $rows->ID;?>" <?php echo  $rows->STTS =='1' ? 'checked' : '' ?>>
															  <?php echo $rows->NM_SUBMENU?> 
															</label>
														</div>
													</li>
											<?php } ?>
										</ul>
										<?php }else{
										} ?>
									</li>
								<?php } ?>
								</ul>
							</div>
						</div><!-- /.row -->
					</div><!-- /.box-body -->
				</form>
			</div><!-- /.box -->
		</div>
		
		<div class="col-md-4 col-sm-12">
			<div class="box box-danger">
				<div class="box-header with-border">
					<h3 class="box-title">Hak Akses Guest</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
					</div>
				</div><!-- /.box-header -->
				<form action="<?php echo site_url('welcome/ubahHakUser') ?>" method="post">
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<ul class="" style='list-style:none'>
								<?php 
									$icon ="fontello-users";
									$level = 3;
									foreach($this->m_menu->getAllMenuAcl($level)->result() as $row){
									?>
									<li class="">
										<li>
											<div class="checkbox">
												<label>
												  <input type="checkbox" name='boxMenuGuest[]' onclick="saveDataAclUser(this.value)" value="<?php echo $row->ID;?>" <?php echo $row->STTS =='1' ? 'checked' : '' ?>  >
												  <?php echo $row->NM_MENU?> 
												</label>
											</div>
										</li>
										<?php $count = $this->m_menu->getCountSubMenu($row->ID_MENU,$level)->result();
											foreach($count as $r){
												$con = $r->jumlah;
											}
											if($con > 0){ ?>
											
										<ul class="" style='list-style:none'>
											<?php
												foreach($this->m_menu->getAllSubMenuAcl($row->ID_MENU,$level)->result() as $rows){
													?>
													<li>
														<div class="checkbox">
															<label>
															  <input type="checkbox" name='boxMenuGuest[]' value="<?php echo $rows->ID;?>" <?php echo  $rows->STTS =='1' ? 'checked' : '' ?> onclick="saveDataAclUser(this.value)">
															  <?php echo $rows->NM_SUBMENU?> 
															</label>
														</div>
													</li>
											<?php } ?>
										</ul>
										<?php }else{
										} ?>
									</li>
								<?php } ?>
								</ul>
							</div>
						</div><!-- /.row -->
					</div><!-- /.box-body -->
				</form>
			</div><!-- /.box -->
		</div>
		
	</div>
</section><!-- /.content -->


<script>

$(document).ready(function(){
	loadDataAclUser();
});

function loadDataAclUser()
{
	
}

function saveDataAclUser(i)
{

	$.ajax({
		url : 'ubahHakAkses',
		data : {i:i},
		type : 'post',
		dataType : 'json',
		beforeSend : function()
		{
			
		},
		success : function(result)
		{
			if(result.message==1)
			{
				alert('hak akses diubah');
			}
			
		}
		
	});
}

</script>