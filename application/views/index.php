<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url()?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url()?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url()?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url()?>dist/css/skins/_all-skins.min.css">
	
	<!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url()?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url()?>bootstrap/js/bootstrap.min.js"></script>
  </head>

  <body class="hold-transition skin-blue sidebar-collapse fixed sidebar-mini">
    <div class="wrapper">

      <?php $this->load->view('layout/vwHeader')?>
      <?php $this->load->view('layout/vwSidebar')?>
      

	  
	<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php $this->load->view($view)?>
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2015 Alta Putra Indomedia, PT | </strong> All rights reserved.
      </footer>

    </div><!-- ./wrapper -->

    
    <!-- FastClick -->
    <script src="<?php echo base_url()?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url()?>dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url()?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- SlimScroll 1.3.0 Menu Scroll-->
    <script src="<?php echo base_url()?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- AdminLTE -->
    <script src="<?php echo base_url()?>dist/js/demo.js"></script>
  </body>
</html>