<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Supplier
            <small>Menejemen Supplier</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('welcome/login')?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $dash; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data Supplier</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
			
            <div class="box-body">
				<?php if($this->session->userdata('level')!=3)
				{
					?>
						<div class="row">
							<div class="col-md-4">
								<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</button>
							</div><!-- /.col -->
						</div><!-- /.row -->
						<br/>
					<?php 
				}?>
			  <div class="row">
				<div class="col-md-12 col-sm-12">
				<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" >
					<thead>
						<tr>
							<th style='text-align:center'>No</th>
							<th>KODE SUPPLIER</th>
							<th>NAMA SUPPLIER</th>
							<th style='text-align:center'>NO TELP</th>
							<th>ALAMAT</th>
							<th style='text-align:center'>AKSI</th>
						</tr>
					</thead>
					<tbody id="tableDataSupplier">
						
					</tbody>
				</table>
				</div>
				<nav>
					<ul class="pagination">
						<li>
						  <a href="#" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						  </a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
						  <a href="#" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						  </a>
						</li>
					</ul>
				</nav>
				</div>
			</div>
            </div><!-- /.box-body -->
          </div><!-- /.box -->

		</section><!-- /.content -->
		
<script>
$(document).ready(function(){
	loadDataSupplier();
});


function loadDataSupplier()
{
	$.ajax({
		url		: 'supplier/getDataSupplier',
		type	: 'POST',
		dataType: 'html',
		beforeSend : function()
		{
			
		},
		success : function(result)
		{
			$('#tableDataSupplier').empty().append(result);
		}
	});
}
</script>