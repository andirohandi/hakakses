<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		if(!$this->session->userdata('haveLogedIn'))
		{
			redirect ('');
		}
		$this->load->model("m_menu");
		$this->load->model("m_supplier");
		$this->load->model("m_verify");
	}

	public function index()
	{
		$level = $this->session->userdata('level');
		$id_menu = 2;
		$id_submenu = 2;
		
		$data = array(
			'ID_LEVEL' => $level,
			'ID_MENU' => $id_menu,
			'ID_SUBMENU' => $id_submenu,
			'STATUS' => 0
		);
		
		$cek = $this->m_verify->cekVisible($data);
		
		if($cek->num_rows()>0)
		{
			redirect('welcome/error');
		}else
		{
			$data['title'] = "Supplier";
			$data['dash'] = "Supplier";
			$data['view'] = "vwSupplier";
			$this->load->view('index',$data);
		}
	}
	
	function getDataSupplier()
	{
		$level = $this->session->userdata('level');
		$qry = $this->m_supplier->getDbDataSupplierAll();
		$no = 0;
		$data = '';
		foreach($qry->result() as $row):$no++;
			
			$data .="<tr>";
			$data .="<td style='text-align:center' >".$no."</td>";
			$data .="<td>".$row->KODE_SUP."</td>";
			$data .="<td>".$row->NAMA_SUP."</td>";
			$data .="<td style='text-align:center' >".$row->NO_TELP."</td>";
			$data .="<td>".$row->ALAMAT_SUP."</td>";
			if($level==1)
			{
				$data .="<td style='text-align:center' ><button class='btn btn-primary btn-sm'><i class='fa fa-edit'></i> </button> <button class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> </button></td>";
			}else if($level==2)
			{
				$data .="<td style='text-align:center' ><button class='btn btn-primary btn-sm'><i class='fa fa-edit'></i> </button><td>";
			}else
			{
			}
			$data .="<tr>";
		endforeach;
		
		echo $data;
		
	}
	
	
}
