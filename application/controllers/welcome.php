<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			$this->load->model("m_verify");
			$this->load->model("m_menu");
	}

	public function index()
	{
		$data['err'] = '';
		$this->load->view('login',$data);
	}
	
	public function beranda()
	{
		$data['title'] = "Pengaturan";
		$data['dash'] = "";
		$data['view'] = "vwBeranda";
		$this->load->view('index',$data);
	}
	
	public function login()
	{
		$username = trim($this->input->post("username"));
		$password = trim($this->input->post("password"));
		
		$dt = array(
			'USERNAME' => $username,
			'PASSWORD' => md5($password)
		);

		$cek = $this->m_verify->cekAuth($dt);
		
		if($cek->num_rows() == 1)
		{
			$dtSession = array();
			
			foreach($cek->result() as $row)
			{	
				$user = $row->USERNAME;
				$pass = $row->PASSWORD;	
				$level = $row->ID_LEVEL;	
			}
			
			
			$this->session->set_userdata(array(
				'haveLogedIn' => true,
				'username'	=> $user,
				'password' => $pass,
				'level' => $level
			));
			
			
			redirect('welcome/beranda');
			
		}else
		{
			$data['err'] = "<div class='alert alert-warning'>Access Denied ! Your Username or Password is Wrong !</div>";
			$this->load->view('login',$data);
		}
	}
	
	public function home()
	{
		$level = $this->session->userdata('level');
		$id_menu = 1;
		$id_submenu = 0;
		
		$data = array(
			'ID_LEVEL' => $level,
			'ID_MENU' => $id_menu,
			'ID_SUBMENU' => $id_submenu,
			'STATUS' => 0
		);
		
		$cek = $this->m_verify->cekVisible($data);
		
		if($cek->num_rows()>0)
		{
			redirect('welcome/error');
		}else
		{
			$data['title'] = "Pengaturan";
			$data['dash'] = "";
			$data['view'] = "home";
			$this->load->view('index',$data);
		}
		
	}
	
	public function register()
	{
		$this->load->view('register');
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('');
	}
	
	public function ubahHakAdmin()
	{
		$jumlah = count($_POST["boxMenuAdmin"]);
		for($i=0; $i < $jumlah; $i++) 
		{
			$id=$_POST["boxMenuAdmin"][$i];
			echo $id."<br/>";
			//$status = 1;
			//$this->db->query("UPDATE tbl_hakakses SET STATUS='".$status."' WHERE ID='".$id."'");
		}
		
		/*$jumlah2 = count($_POST["boxMenuAdminUnchk"]);
		for($a=0; $a < $jumlah2; $a++) 
		{
			$id=$_POST["boxMenuAdminUnchk"][$a];
			echo $id."<br/>";
			//$status = 1;
			//$this->db->query("UPDATE tbl_hakakses SET STATUS='".$status."' WHERE ID='".$id."'");
		}*/
		
		redirect('welcome/home');

	}
	
	public function ubahHakAkses()
	{
		$id = $_POST['i'];
		
		$cek = $this->m_menu->cekStatus($id);
		$stt = '';
		$status = '';
		
		foreach($cek->result() as $row)
		{
			$status = $row->STATUS;
			if($status==1)
			{
				$stt = 0;
			}else
			{
				$stt = 1;
			}
			
		}
		
		
		$this->m_menu->updateStatus($id,$stt);
		
		$dt = array('message'=>1);
		
		echo json_encode($dt);
	}
	
	
	public function error()
	{
		$data['title'] = "Akses Forbidden";
		$this->load->view('errors/error',$data);
	}
}
