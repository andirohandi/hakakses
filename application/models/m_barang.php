<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_barang extends CI_Model {

	private $table = "tbl_barang";
	private $id = "ID";
	
	function getDbDataBarangAll()
	{
		$this->db->from('tbl_barang a');
		$this->db->join('tbl_supplier b', 'a.SUPPLIER=b.ID','left');
		return $this->db->get();
	}
	
	
}