<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_menu extends CI_Model {

	private $table = "tbl_hakakses";
	private $id = "ID";
	
	function getAllMenu($level)
	{
		//$level = 1;
		$data = array(
			'a.STATUS' 	=> 1,
			'a.ID_LEVEL' 	=> $level, //nantinya diubah sesuai user
			'a.KATEGORI'	=> 1
		);
		
		$this->db->select("a.*, b.*", FALSE);
		$this->db->select("a.ID_MENU AS ID_MENU", FALSE);
		$this->db->from("tbl_hakakses a");
		$this->db->join('tbl_menu b', 'a.ID_MENU=b.ID_MENU','left');
		$this->db->where($data);
		$this->db->order_by('b.NM_MENU', 'ASC');
		return $this->db->get();
	}
	
	function getAllSubMenu($id_menu,$level)
	{
		//$level = 1;
		$data = array(
			'a.STATUS' 	=> 1,
			'a.ID_LEVEL' 	=> $level, //nantinya diubah sesuai user
			'a.KATEGORI'	=> 2,
			'a.ID_MENU'	=> $id_menu
		);
		
		$this->db->select("a.*, c.*", FALSE);
		$this->db->select("c.ICON AS ICON_SUBMENU", FALSE);
		$this->db->from("tbl_hakakses a");
		$this->db->join('tbl_submenu c', 'a.ID_SUBMENU=c.ID_SUBMENU','left');
		$this->db->where($data);
		
		
		return $this->db->get();
	}
	
	function getAllMenuAcl($level)
	{
		//$level = 1;
		$data = array(
			'a.ID_LEVEL' 	=> $level, //nantinya diubah sesuai user
			'a.KATEGORI'	=> 1
		);
		
		$this->db->select("a.*, b.*", FALSE);
		$this->db->select("a.ID_MENU AS ID_MENU, a.STATUS AS STTS", FALSE);
		$this->db->from("tbl_hakakses a");
		$this->db->join('tbl_menu b', 'a.ID_MENU=b.ID_MENU','left');
		$this->db->where($data);
		
		return $this->db->get();
	}
	
	function getAllSubMenuAcl($id_menu,$level)
	{
		//$level = 1;
		$data = array(
			'a.ID_LEVEL' 	=> $level, //nantinya diubah sesuai user
			'a.KATEGORI'	=> 2,
			'a.ID_MENU'	=> $id_menu
		);
		
		$this->db->select("a.*, c.*", FALSE);
		$this->db->select("c.ICON AS ICON_SUBMENU, a.STATUS AS STTS", FALSE);
		$this->db->from("tbl_hakakses a");
		$this->db->join('tbl_submenu c', 'a.ID_SUBMENU=c.ID_SUBMENU','left');
		$this->db->where($data);
		
		return $this->db->get();
	}
	
	public function cekStatus($id)
	{
		$this->db->WHERE('ID',$id);
		return $this->db->get($this->table);
	}
	
	public function updateStatus($id,$stt)
	{
		$this->db->set('STATUS',$stt);
		$this->db->where('ID',$id);
		$this->db->update($this->table);
	}
	
	
	function getCountSubMenu($id_menu,$level){
		//$level = 1;
		return $this->db->query("SELECT COUNT(ID) AS jumlah FROM tbl_hakakses WHERE (ID_MENU='".$id_menu."' AND ID_SUBMENU != 0) AND ID_LEVEL ='".$level."'");
	}
}