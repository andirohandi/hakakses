<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_verify extends CI_Model {

	private $table = "tbl_user";
	private $id = "ID";
	
	public function cekAuth($dt)
	{
		$this->db->where($dt);
		return $this->db->get($this->table);
	}
	
	public function cekVisible($dt)
	{
		$this->db->where($dt);
		return $this->db->get('tbl_hakakses');
	}
}