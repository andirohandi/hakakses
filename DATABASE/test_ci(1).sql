-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Sep 2015 pada 02.57
-- Versi Server: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_ci`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_barang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KODE_BRG` varchar(20) NOT NULL,
  `NAMA_BRG` varchar(50) NOT NULL,
  `KEMASAN_BRG` varchar(10) NOT NULL,
  `SATUAN_BRG` int(11) NOT NULL,
  `HARGA_BRG` int(11) NOT NULL,
  `SUPPLIER` int(11) NOT NULL,
  `STATUS` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tbl_barang`
--

INSERT INTO `tbl_barang` (`ID`, `KODE_BRG`, `NAMA_BRG`, `KEMASAN_BRG`, `SATUAN_BRG`, `HARGA_BRG`, `SUPPLIER`, `STATUS`) VALUES
(1, 'BRG-001', 'MONITOR LG 20 INC', 'UNIT', 1, 1500000, 1, 1),
(2, 'BRG-002', 'MONITOR SAMSUNG 22 INC', 'UNIT', 1, 1670000, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hakakses`
--

CREATE TABLE IF NOT EXISTS `tbl_hakakses` (
  `ID` tinyint(4) NOT NULL AUTO_INCREMENT,
  `ID_MENU` tinyint(4) NOT NULL,
  `ID_SUBMENU` tinyint(4) NOT NULL,
  `KATEGORI` tinyint(1) NOT NULL,
  `ID_LEVEL` tinyint(4) NOT NULL,
  `STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data untuk tabel `tbl_hakakses`
--

INSERT INTO `tbl_hakakses` (`ID`, `ID_MENU`, `ID_SUBMENU`, `KATEGORI`, `ID_LEVEL`, `STATUS`) VALUES
(1, 1, 0, 1, 1, 1),
(2, 2, 0, 1, 1, 1),
(3, 2, 1, 2, 1, 1),
(4, 2, 2, 2, 1, 1),
(5, 3, 0, 1, 1, 1),
(6, 3, 3, 2, 1, 1),
(7, 3, 4, 2, 1, 1),
(8, 1, 0, 1, 2, 0),
(9, 2, 0, 1, 2, 1),
(10, 2, 1, 2, 2, 1),
(11, 2, 2, 2, 2, 1),
(12, 3, 0, 1, 2, 0),
(13, 3, 3, 2, 2, 0),
(14, 3, 4, 2, 2, 0),
(15, 1, 0, 1, 3, 0),
(16, 2, 0, 1, 3, 1),
(17, 2, 1, 2, 3, 1),
(18, 2, 2, 2, 3, 0),
(19, 3, 0, 1, 3, 1),
(20, 3, 3, 2, 3, 1),
(21, 3, 4, 2, 3, 1),
(22, 4, 0, 1, 1, 1),
(23, 4, 0, 1, 2, 1),
(24, 4, 0, 1, 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_level`
--

CREATE TABLE IF NOT EXISTS `tbl_level` (
  `ID_LEVEL` int(11) NOT NULL AUTO_INCREMENT,
  `NM_LEVEL` varchar(50) NOT NULL,
  `STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID_LEVEL`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `tbl_level`
--

INSERT INTO `tbl_level` (`ID_LEVEL`, `NM_LEVEL`, `STATUS`) VALUES
(1, 'admin', 1),
(2, 'user', 2),
(3, 'guest', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `ID_MENU` int(11) NOT NULL AUTO_INCREMENT,
  `NM_MENU` varchar(50) NOT NULL,
  `URL_MENU` varchar(50) NOT NULL,
  `STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID_MENU`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `tbl_menu`
--

INSERT INTO `tbl_menu` (`ID_MENU`, `NM_MENU`, `URL_MENU`, `STATUS`) VALUES
(1, 'HAK AKSES', 'welcome/home', 1),
(2, 'MASTER', '#', 1),
(3, 'TRANSAKSI', '#', 1),
(4, 'BERANDA', 'welcome/beranda', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_submenu`
--

CREATE TABLE IF NOT EXISTS `tbl_submenu` (
  `ID_SUBMENU` int(11) NOT NULL AUTO_INCREMENT,
  `NM_SUBMENU` varchar(50) NOT NULL,
  `URL` varchar(30) NOT NULL,
  `ICON` varchar(50) NOT NULL,
  `ID_MENU` tinyint(4) NOT NULL,
  `STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID_SUBMENU`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `tbl_submenu`
--

INSERT INTO `tbl_submenu` (`ID_SUBMENU`, `NM_SUBMENU`, `URL`, `ICON`, `ID_MENU`, `STATUS`) VALUES
(1, 'Master Barang', 'barang', '', 2, 1),
(2, 'Master Supplier', 'supplier', '', 2, 1),
(3, 'Trans Pembelian', 'pembelian', '', 3, 1),
(4, 'Trans Penjualan', 'penjualan', '', 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_supplier`
--

CREATE TABLE IF NOT EXISTS `tbl_supplier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KODE_SUP` varchar(20) NOT NULL,
  `NAMA_SUP` varchar(50) NOT NULL,
  `NO_TELP` varchar(14) NOT NULL,
  `ALAMAT_SUP` text NOT NULL,
  `STATUS` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`ID`, `KODE_SUP`, `NAMA_SUP`, `NO_TELP`, `ALAMAT_SUP`, `STATUS`) VALUES
(1, 'SP-001', 'BASF PT', '0222222222', 'Surabaya ', 1),
(2, 'SP-002', 'SAN DARMA PT', '023232323', 'Padalarang, Bandung barat', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `ID_USER` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `ID_LEVEL` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID_USER`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`ID_USER`, `USERNAME`, `PASSWORD`, `ID_LEVEL`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 2),
(3, 'guest', '084e0343a0486ff05530df6c705c8bb4', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
